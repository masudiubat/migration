<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class managementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

        for($i=0;$i<50;$i++){

            $user=array(

                array(

                    'name'=>$faker->name,
                    'email'=>$faker->email,
                    'designation'=>$faker->name,
                    'title'=>$faker->name,
                )
            );

            DB::table('managements')->insert($user);

        }


    }
}
