<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class graphicmanagementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('graphicmanagements')->insert(
            factory(App\graphicmanagements::class,50)->create()
        );

    }
}
